#include "Operands/IOperand.h"

#include <string>

class OperandFactory
{
  public:
  IOperand const * createOperand( eOperandType type, std::string const & value ) const;
};